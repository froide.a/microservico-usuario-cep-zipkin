package br.com.itau.usuariocep.Repository;


import br.com.itau.usuariocep.model.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

}
