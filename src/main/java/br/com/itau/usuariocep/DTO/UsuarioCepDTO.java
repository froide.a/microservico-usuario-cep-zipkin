package br.com.itau.usuariocep.DTO;

public class UsuarioCepDTO {

    private  String nome;
    public String cep;


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

}
