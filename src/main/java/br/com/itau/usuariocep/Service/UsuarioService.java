package br.com.itau.usuariocep.Service;


import br.com.itau.usuariocep.DTO.UsuarioCepDTO;
import br.com.itau.usuariocep.Repository.UsuarioRepository;
import br.com.itau.usuariocep.client.Cep;
import br.com.itau.usuariocep.client.UsuarioCep;
import br.com.itau.usuariocep.model.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    UsuarioCep usuarioCep;


    public Usuario salvarUsuario(UsuarioCepDTO usuarioCepDTO) {



        Cep cep = usuarioCep.buscarCep(usuarioCepDTO.cep);

        Usuario usuario = new Usuario();

        usuario.setNome(usuarioCepDTO.getNome());
        usuario.setCep(cep.getCep());
        usuario.setLogradouro(cep.getLogradouro());
        usuario.setBairro(cep.getBairro());
        usuario.setLocalidade(cep.getLocalidade());
        usuario.setUf(cep.getUf());

        return usuarioRepository.save(usuario);

    }
}
