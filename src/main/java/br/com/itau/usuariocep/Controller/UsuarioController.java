package br.com.itau.usuariocep.Controller;


import br.com.itau.usuariocep.DTO.UsuarioCepDTO;
import br.com.itau.usuariocep.Service.UsuarioService;
import br.com.itau.usuariocep.model.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/usuariocep")
public class UsuarioController {

@Autowired
    UsuarioService usuarioService;

    @GetMapping("/{nome}/{cep}")
     public Usuario criarUsuario(@PathVariable String nome, @PathVariable String cep) {

        UsuarioCepDTO usuarioCepDTO = new UsuarioCepDTO();

        usuarioCepDTO.setNome(nome);
        usuarioCepDTO.setCep(cep);

        return usuarioService.salvarUsuario(usuarioCepDTO);

    }

}
