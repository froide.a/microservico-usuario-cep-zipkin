package br.com.itau.usuariocep.client;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name= "cep")
public interface UsuarioCep {

    @GetMapping("/cep/{cep}")
    Cep buscarCep(@PathVariable String cep);


}
